%define BUFFER_SIZE 256
extern read_word					
extern print_newline			
extern print_newline_stderr
extern print_string			
extern string_length	
extern find_word	
extern print_error
extern exit		



global _start


section .data		
notFound: db "Ничего не найдено", 0	
overflow: db 'Больше чем 255 символов', 0	

%include "words.inc"
section .text		


_start:			
    sub rsp, BUFFER_SIZE		
    mov rdi, rsp			
    mov rsi, BUFFER_SIZE
    call read_word		
    test rax, rax	
    je .not_enough
    mov rdi, rsp
    mov rsi, next				
    call find_word			
    add rsp, BUFFER_SIZE
    test rax, rax		
    je .not_found	
    add rax, 8					
    mov rdi, rax			
    push rax			
    call string_length
    pop rdi			
    add rdi, rax
    inc rdi	
    call print_string		
    call print_newline
    mov rdi, 0
    call exit

.not_enough:			
    mov rdi, overflow	
    call print_error
    call print_newline_stderr
    jmp .end	

.not_found:	
    mov rdi, notFound
    call print_error
    call print_newline_stderr

.end:				
    mov rdi, 1
    call exit	

