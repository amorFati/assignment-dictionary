global find_word			

extern string_equals	

section .text		
find_word: 				
    .loop:
    cmp rsi, 0
	jz .failed

	push rdi
	push rsi
	add rsi, 8
	call string_equals
	pop rsi			
	pop rdi		
	test rax, rax	
	jne .found

	mov rsi, [rsi]			
	jmp .loop	
    .found:			
	mov rax, rsi
	ret		
    .failed:
	xor rax, rax		
	ret     		

